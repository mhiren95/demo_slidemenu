//
//  AppDelegate.swift
//  Slidemenu_tabbar
//
//  Created by tops on 27/11/18.
//  Copyright © 2018 tops. All rights reserved.
//

import UIKit
import  MVYSideMenu
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var firstVC : FirstVC?
    var secondVC:SecondVC?
    var thirdVC:ThirdVC?
    var fourthVC:FourthVC?
    var fifthVC:FifthVC?
    var tabBarCustomObj:UITabBarCustom?
    var storyboard:UIStoryboard?
    var navObj:UINavigationController?
    var SideMenu:MVYSideMenuController?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        window = UIWindow.init(frame: UIScreen.main.bounds)
        self.firstVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstVC") as? FirstVC
        navObj = UINavigationController.init(rootViewController: self.firstVC!)
        self.navObj?.navigationBar.isHidden = true
        SetTabbar()
        return true
    }
    func SetTabbar(){
        self.tabBarCustomObj = UITabBarCustom(nibName: "UITabBarCustom", bundle: nil)
        self.firstVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstVC") as? FirstVC
        self.secondVC = self.storyboard?.instantiateViewController(withIdentifier: "SecondVC") as? SecondVC
        self.thirdVC = self.storyboard?.instantiateViewController(withIdentifier: "ThirdVC") as? ThirdVC

        self.fourthVC = self.storyboard?.instantiateViewController(withIdentifier: "FourthVC") as? FourthVC

        self.fifthVC = self.storyboard?.instantiateViewController(withIdentifier: "FifthVC") as? FifthVC
        let navTimelineObj: UINavigationController = UINavigationController.init(rootViewController: firstVC!)
        
        let navSearchObj: UINavigationController = UINavigationController.init(rootViewController: secondVC!)
        
        let navCreatePostObj: UINavigationController = UINavigationController.init(rootViewController: thirdVC!)
        
        let navNotificationObj: UINavigationController = UINavigationController.init(rootViewController: fourthVC!)
        
        let navCartListObj: UINavigationController = UINavigationController.init(rootViewController: fifthVC!)
        
        self.tabBarCustomObj?.viewControllers = [navTimelineObj, navSearchObj, navCreatePostObj, navNotificationObj, navCartListObj]
        SetSlideMenu()
        self.navObj?.pushViewController(self.tabBarCustomObj!, animated: false)
    }
    func SetSlideMenu(){
        let VC = SlideMenuVC(nibName: "SlideMenuVC", bundle: nil) as? SlideMenuVC
        SideMenu = MVYSideMenuController(menuViewController: VC, contentViewController: self.tabBarCustomObj)
        window?.rootViewController = SideMenu;
        window?.makeKeyAndVisible()
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

