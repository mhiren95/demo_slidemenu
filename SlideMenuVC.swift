//
//  SlideMenuVC.swift
//  Slidemenu_tabbar
//
//  Created by tops on 27/11/18.
//  Copyright © 2018 tops. All rights reserved.
//

import UIKit

class SlideMenuVC: UIViewController {
    @IBOutlet weak var tblSlideMenu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSlideMenu.delegate = self
        tblSlideMenu.dataSource = self
        
        tblSlideMenu.register(UINib(nibName: "SlideMenuCell", bundle: nil), forCellReuseIdentifier: "SlideMenuCell")

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SlideMenuVC:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var Cell = tableView.dequeueReusableCell(withIdentifier: "SlideMenuCell") as! SlideMenuCell
        Cell.lblTitle.text = "Select \(indexPath.row + 1)"
        return Cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Global.appDelegate.SideMenu?.removeFromParent()
        Global.appDelegate.SideMenu?.closeMenu()
        switch indexPath.row {
        case 0:
            Global.appDelegate.tabBarCustomObj?.selectTab(0)
            
        case 1:
            Global.appDelegate.tabBarCustomObj?.selectTab(1)
        case 2:
            Global.appDelegate.tabBarCustomObj?.selectTab(2)
        case
            3:
            Global.appDelegate.tabBarCustomObj?.selectTab(3)
        case 4:
            Global.appDelegate.tabBarCustomObj?.selectTab(4)
        default:
            Global.appDelegate.tabBarCustomObj?.selectTab(0)
        }
    }
}
