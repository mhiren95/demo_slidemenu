//
//  UITabBarCustom.swift
//  chilax
//
//  Created by Tops on 6/27/17.
//  Copyright © 2017 Tops. All rights reserved.
//

import UIKit

class UITabBarCustom: UITabBarController, UITabBarControllerDelegate {
   // var lblShadowLine: UILabel?
    
    var btnTab1: UIButton?
    var btnTab2: UIButton?
    var btnTab3: UIButton?
    var btnTab4: UIButton?
    var btnTab5: UIButton?
    var Vw_indicator:UIView = UIView()
    var floatTabAspRatio: CGFloat?
    
    // MARK: -  View Life Cycle Start Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideOriginalTabBar()
        self.addCustomElements()
       // self.view.backgroundColor = Global.singleton.SetTextcolor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: -  Hide Original Add Custom elements Method
    func hideOriginalTabBar() {
        for view in self.tabBar.subviews {
            view.isHidden = true
        }
        
        for view in self.view.subviews {
            if view is UITabBar {
                view.isHidden = true
                view.removeFromSuperview()
                break;
            }
        }
    }
    
    func addCustomElements() {
        floatTabAspRatio = Global.screenWidth/CGFloat(320)
        
        self.removeAllOldElements()
    }
    func ChangeTabbarWithStatus(){
        self.removeAllOldElements()
        self.addAllElements()
    }
    func removeAllOldElements(isExtraHeightRequired : Bool = false) {
        if (btnTab1 != nil) {
            btnTab1?.removeFromSuperview()
        }
        if (btnTab2 != nil) {
            btnTab2?.removeFromSuperview()
        }
        if (btnTab3 != nil) {
            btnTab3?.removeFromSuperview()
        }
        if (btnTab4 != nil) {
            btnTab4?.removeFromSuperview()
        }
        if (btnTab5 != nil) {
            btnTab5?.removeFromSuperview()
        }
        if (Vw_indicator != nil){
            Vw_indicator.removeFromSuperview()
        }
        
        self.addAllElements(isExtraHeightRequired: isExtraHeightRequired)
    }
    func addAllElements(isExtraHeightRequired : Bool = false) {
        if UIScreen.main.nativeBounds.height == 2436
        {
           // lblShadowLine = UILabel(frame: CGRect(x: 0, y: (Global.screenHeight - (42 * floatTabAspRatio!))-30, width: Global.screenWidth, height: 1))
        }
        else
        {
          //  lblShadowLine = UILabel(frame: CGRect(x: 0, y: Global.screenHeight - (42 * floatTabAspRatio!), width: Global.screenWidth, height: 1))
        }

       // lblShadowLine?.layer.shadowColor = UIColor.darkGray.cgColor
       // lblShadowLine?.layer.shadowOpacity = 0.1
       // lblShadowLine?.layer.shadowOffset = CGSize.zero
       // lblShadowLine?.layer.shadowRadius = 1
       // lblShadowLine?.layer.shadowPath = UIBezierPath(rect: (lblShadowLine?.bounds)!).cgPath
       // self.view.addSubview(lblShadowLine!)
        
        btnTab1 = self.generateTabButton(0, isSelected: false,isExtraHeightRequired: isExtraHeightRequired)
        btnTab2 = self.generateTabButton(1, isSelected: false,isExtraHeightRequired: isExtraHeightRequired)
        btnTab3 = self.generateTabButton(2, isSelected: false,isExtraHeightRequired: isExtraHeightRequired)
        btnTab4 = self.generateTabButton(3, isSelected: false,isExtraHeightRequired: isExtraHeightRequired)
        btnTab5 = self.generateTabButton(4, isSelected: false,isExtraHeightRequired: isExtraHeightRequired)
        
        self.view.addSubview(btnTab1!)
        self.view.addSubview(btnTab2!)
        self.view.addSubview(btnTab3!)
        self.view.addSubview(btnTab4!)
        self.view.addSubview(btnTab5!)
        self.view.addSubview(Vw_indicator)
        Vw_indicator.isHidden = true
        self.view.backgroundColor = UIColor.blue

        btnTab1?.addTarget(self, action: #selector(UITabBarCustom.buttonClicked(_:)), for: .touchUpInside)
        btnTab2?.addTarget(self, action: #selector(UITabBarCustom.buttonClicked(_:)), for: .touchUpInside)
        btnTab3?.addTarget(self, action: #selector(UITabBarCustom.buttonClicked(_:)), for: .touchUpInside)
        btnTab4?.addTarget(self, action: #selector(UITabBarCustom.buttonClicked(_:)), for: .touchUpInside)
        btnTab5?.addTarget(self, action: #selector(UITabBarCustom.buttonClicked(_:)), for: .touchUpInside)

//        let rect: CGRect = UIApplication.shared.statusBarFrame
//        if (rect.size.height > 20) {
//            self.changeFrameOfAllCustomElementForStatusBarHeightChange(2)
//        }
    }

    func generateTabButton(_ intTag: Int, isSelected boolSel: Bool,isExtraHeightRequired : Bool = false) -> UIButton {
        let btn = UIButton(type: .custom)
        btn.tag = intTag
        btn.isSelected = boolSel
        btn.titleLabel?.font = UIFont(name: Global.kFont.ChilaxIcon, size: Global.singleton.getDeviceSpecificFontSize(19))
        //btn?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
        btn.setTitleColor(Global.kAppColor.Blue, for: .highlighted)
        var tempY:CGFloat = 0.0
        //2,688
        if UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2688
        {
                tempY = (Global.screenHeight - (42 * floatTabAspRatio!)) - 10
        }
        else
            
        {
            tempY =  (Global.screenHeight - (42 * floatTabAspRatio!))
           // tempY = isExtraHeightRequired ? ((Global.screenHeight - (42 * floatTabAspRatio!))-5) : (Global.screenHeight - (42 * floatTabAspRatio!))
        }
        let tempBtnHeight: CGFloat = 42 * floatTabAspRatio!
        let tempBtnWidth: CGFloat = Global.screenWidth / 5
        switch intTag {
        case 0: //Tab-1
            btn.setTitle("One", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 0), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
            self.Vw_indicator.frame = CGRect(x: (tempBtnWidth/2.0)-3.5, y: tempY+4, width: 6, height: 6)
            self.Vw_indicator.backgroundColor = Global.kAppColor.Blue
            self.Vw_indicator.layer.cornerRadius = 4.5
            btn.setTitleColor(Global.kAppColor.Blue, for: .normal)
            btn.setTitleColor(Global.singleton.SetTextcolor(), for: .highlighted)
            
        case 1: //Tab-2
            btn.setTitle("Two", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 1), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
        case 2: //Tab-3
            btn.setTitle("Three", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 2), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
           
        case 3: //Tab-4
            btn.setTitle("Four", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 3), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
        case 4: //Tab-5
            btn.setTitle("Five", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 4), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
            btn.titleLabel?.font = UIFont(name: Global.kFont.ChilaxIcons, size: Global.singleton.getDeviceSpecificFontSize(18))
            
        default:
            btn.setTitle("1", for: .normal)
            btn.frame = CGRect(x: 0 + (tempBtnWidth * 0), y: tempY, width: tempBtnWidth, height: tempBtnHeight)
        }

        return btn;
    }
    
    // MARK: -  Button Click Methods
    @objc func buttonClicked (_ sender: UIButton) {
            self.selectTab(sender.tag)
    }
    
    // MARK: -  Tab bar selection
    func selectTab (_ intSelTabId: Int) {

        if (self.selectedIndex == intSelTabId) {
            if intSelTabId == 0
            {
                let notificationName = Notification.Name("ScrollToFirst")
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
            let navController: UINavigationController = (self.selectedViewController as? UINavigationController)!
            navController.popToRootViewController(animated: true)
        }
        else {
            if (intSelTabId == 2) {
            }
            self.selectedIndex = intSelTabId
        }
        
        self.setTabbaritemColor(index: intSelTabId, newcolorIndex:intSelTabId)
    }
    func setTabbaritemColor(index:Int, newcolorIndex:Int)
    {
        btnTab1?.isSelected = false
        btnTab2?.isSelected = false
        btnTab3?.isSelected = false
        btnTab4?.isSelected = false
        btnTab5?.isSelected = false
        
        if index == 0
        {
                btnTab1?.isSelected = true
            
                btnTab1?.setTitleColor(Global.kAppColor.Blue, for: .normal)
            btnTab2?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab3?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab4?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab5?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
        }
        else if index == 1
        {
            btnTab1?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab2?.setTitleColor(Global.kAppColor.Blue, for: .normal)
            btnTab3?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab4?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab5?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab2?.isSelected = true
        }
        else if index == 2
        {
            btnTab1?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab2?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab3?.setTitleColor(Global.kAppColor.Blue, for: .normal)
            btnTab4?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab5?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab3?.isSelected = true
        }
        else if index == 3
        {
            btnTab1?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab2?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab3?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab4?.setTitleColor(Global.kAppColor.Blue, for: .normal)
            btnTab5?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab4?.isSelected = true
        }
        else{
            btnTab1?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab2?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab3?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            btnTab4?.setTitleColor(Global.singleton.SetTextcolor(), for: .normal)
            
                btnTab5?.setTitleColor(Global.kAppColor.Blue, for: .normal)
            
                btnTab5?.isSelected = true
        }
    }

    // MARK: -  Hide Show Tabbar
    func addIndcator()
    {
        if btnTab1?.isHidden == false{
            Vw_indicator.isHidden = false
        }
    }
    func removeIndicator(){
        Vw_indicator.isHidden = true
    }
    func showTabBar() {
       // lblShadowLine?.isHidden = true
        
        btnTab1?.isHidden = false
        btnTab2?.isHidden = false
        btnTab3?.isHidden = false
        btnTab4?.isHidden = false
        btnTab5?.isHidden = false
        
        btnTab1?.isUserInteractionEnabled = true
        btnTab2?.isUserInteractionEnabled = true
        btnTab3?.isUserInteractionEnabled = true
        btnTab4?.isUserInteractionEnabled = true
        btnTab5?.isUserInteractionEnabled = true
    }
    
    func hideTabBar() {
       // lblShadowLine?.isHidden = true
        
        btnTab1?.isHidden = true
        btnTab2?.isHidden = true
        btnTab3?.isHidden = true
        btnTab4?.isHidden = true
        btnTab5?.isHidden = true
        Vw_indicator.isHidden = true
        
        btnTab1?.isUserInteractionEnabled = false
        btnTab2?.isUserInteractionEnabled = false
        btnTab3?.isUserInteractionEnabled = false
        btnTab4?.isUserInteractionEnabled = false
        btnTab5?.isUserInteractionEnabled = false
    }
    
    func Tab_changeFrameOfAllCustomElementForStatusBarHeightChange(_ flag:Int,Height : CGFloat = 0) {
        var tempIncDec: CGFloat = Height;
        if (flag == 1) {
            tempIncDec = Height
        }
        else if (flag == 2) {
            print(btnTab1!.frame.origin.y)
            tempIncDec = Height;
        }
        
        self.btnTab1?.frame = CGRect(x: btnTab1!.frame.origin.x, y: Height, width: self.btnTab1!.frame.size.width, height: self.btnTab1!.frame.size.height)
        
        
        self.btnTab2?.frame = CGRect(x: btnTab2!.frame.origin.x, y: Height, width: self.btnTab2!.frame.size.width, height: self.btnTab2!.frame.size.height)
        
        self.btnTab3?.frame = CGRect(x: btnTab3!.frame.origin.x, y: Height, width: self.btnTab3!.frame.size.width, height: self.btnTab3!.frame.size.height)
        
        self.btnTab4?.frame = CGRect(x: btnTab4!.frame.origin.x, y: Height, width: self.btnTab4!.frame.size.width, height: self.btnTab4!.frame.size.height)
        
        self.btnTab5?.frame = CGRect(x: btnTab5!.frame.origin.x, y: Height, width: self.btnTab5!.frame.size.width, height: self.btnTab5!.frame.size.height)
    }
    
    func changeFrameOfAllCustomElementForStatusBarHeightChange(_ flag:Int,Height : CGFloat = 0) {
        var tempIncDec: CGFloat = Height;
        if (flag == 1) {
            tempIncDec = Height
        }
        else if (flag == 2) {
            print(btnTab1!.frame.origin.y)
            tempIncDec = Height;
        }
        
        self.btnTab1?.frame = CGRect(x: btnTab1!.frame.origin.x, y: btnTab1!.frame.origin.y + tempIncDec, width: self.btnTab1!.frame.size.width, height: self.btnTab1!.frame.size.height)
        
        
        self.btnTab2?.frame = CGRect(x: btnTab2!.frame.origin.x, y: btnTab2!.frame.origin.y + tempIncDec, width: self.btnTab2!.frame.size.width, height: self.btnTab2!.frame.size.height)
        
        self.btnTab3?.frame = CGRect(x: btnTab3!.frame.origin.x, y: btnTab3!.frame.origin.y + tempIncDec, width: self.btnTab3!.frame.size.width, height: self.btnTab3!.frame.size.height)
        
        self.btnTab4?.frame = CGRect(x: btnTab4!.frame.origin.x, y: btnTab4!.frame.origin.y + tempIncDec, width: self.btnTab4!.frame.size.width, height: self.btnTab4!.frame.size.height)
        
        self.btnTab5?.frame = CGRect(x: btnTab5!.frame.origin.x, y: btnTab5!.frame.origin.y + tempIncDec, width: self.btnTab5!.frame.size.width, height: self.btnTab5!.frame.size.height)
    }
}
