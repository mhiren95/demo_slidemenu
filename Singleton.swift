                                          //
//  Singleton.swift
//  chilax
//
//  Created by Tops on 6/13/17.
//  Copyright © 2017 Tops. All rights reserved.
//

import UIKit
import AVFoundation
                                        
extension UIScrollView {
func scrollToBottom(animated: Bool) {
    if self.contentSize.height < self.bounds.size.height { return }
    let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
       }
    }

class Singleton: NSObject {
    static let sharedSingleton = Singleton()
    var newUser = (username: "",password: "",name: "",email: "")

    var strSelCountryCode: String?
    var strCaptureImageResRatio: String?
    
    var currentChatWithUser : String = ""
    var currentScreenName : String = ""
    var boolIsChatSubViewAppear: Bool = false
    var strChatUserId: String = ""
    //MARK: -  download Manager for Image
    
    // MARK: -  Skip Backup Attribute for File
    func addSkipBackupAttributeToItemAtURL(URL:NSURL) {
        assert(FileManager.default.fileExists(atPath: URL.path!))
        do {
            try URL.setResourceValue(true, forKey: URLResourceKey.isExcludedFromBackupKey)
        }
        catch let error as NSError {
            print("Error excluding \(URL.lastPathComponent!) from backup \(error)");
        }
    }
    
    // MARK: -  Device Specific Method
    func getDeviceSpecificNibName(_ strNib: String) -> String {
        if Global.is_iPhone._4 {
            return strNib + ("_4")
        }
        else {
            return strNib
        }
    }
    
    func getDeviceSpecificFontSize(_ fontsize: CGFloat) -> CGFloat {
        return ((Global.screenWidth) * fontsize) / 320
    }
    func getDeviceSpecificFontSize_2(_ fontsize: CGFloat) -> CGFloat {
        return ((Global.screenWidth) * fontsize) / 375
    }
    
    // MARK: -  UserDefaults Methods
    func saveToUserDefaults (value: String, forKey key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value , forKey: key as String)
        defaults.synchronize()
    }
    func saveDateToUserDefaults (value: Date, forKey key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value , forKey: key as String)
        defaults.synchronize()
    }

    func saveAnyToUserDefaults (value: Int, forKey key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value , forKey: key as String)
        defaults.synchronize()
    }

    func Numericalformat(count:Int) -> String {
        if count > 100000 {
            return String(Int(count / 1000))+"k"
        }
        else{
            return String(count)
        }
    }
    func retriveDateFromUserDefaults (key: String) -> Date? {
        let defaults = UserDefaults.standard
        if let strVal = defaults.value(forKey: key as String) {
            return strVal as? Date
        }
        else{
            return Date()
        }
    }

    func retriveFromUserDefaults (key: String) -> String? {
        let defaults = UserDefaults.standard
        if let strVal = defaults.string(forKey: key as String) {
            return strVal
        }
        else{
            return "" as String?
        }
    }
    func retriveIntFromUserDefaults (key: String) -> Int? {
        let defaults = UserDefaults.standard
        if let strVal = defaults.value(forKey: key as String) {
            return strVal as! Int
        }
        else{
            return 0 as Int?
            
        }
    }

    
    // MARK: -  String RemoveNull and Trim Method
    func removeNull (str:String) -> String {
        if (str == "<null>" || str == "(null)" || str == "N/A" || str == "n/a" || str.isEmpty) {
            return ""
        } else {
            return str
        }
    }
    
    func kTRIM(string: String) -> String {
        return string.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    // MARK: -  Get string size Method
    
    func getSizeFromAttributedString (str: NSAttributedString, stringWidth width: CGFloat, maxHeight mHeight: CGFloat) -> CGSize {
        let rect : CGRect = str.boundingRect(with: CGSize(width: width, height: mHeight), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        return rect.size
    }
    
    // MARK: -  Attributed String
    
    
    // MARK: -  TextField Validation Method
    func validateEmail(strEmail: String) -> Bool {
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: strEmail)
    }
    
    func validatePhoneNumber(strPhone: String) -> Bool {
        let phoneRegex: String = "[0-9]{8}([0-9]{1,3})?"
        let test = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return test.evaluate(with: strPhone)
    }
    
    // MARK: -  Show Message on Success and Failure
    
    // MARK: -  Emoji encode decode string
    func emojisDecodedConvertedString(strText: String) -> String {
        let dataDecode: Data = strText.data(using: .utf8)!
        if (String.init(data: dataDecode, encoding: .nonLossyASCII) == nil){
            return strText
        }
        return String.init(data: dataDecode, encoding: .nonLossyASCII)!
    }                               
    
    func emojisEncodedConvertedString(strText: String) -> String {
        let dataEncode: Data = strText.data(using: .nonLossyASCII)!
        return String.init(data: dataEncode, encoding: .utf8)!
    }
    
    // MARK: -  get Directory Path
    func getDocumentDirPath() -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        return documentsPath
    }
    //MARK:- Check if account added or not
    func IsAccountAdded(strUserID:String) -> Bool{
        if let MyAccounts = UserDefaults.standard.value(forKey: "MyAccounts") as? [NSDictionary]
        {
            let filteredObj = MyAccounts.filter({$0.object(forKey: "id") as? Int == Int(strUserID)})
            if filteredObj.count > 0{
                return true
            }
        }
        return false
        
    }
    
    //MARK:- ChangeUserDefaultData
    // MARK: -  get Country JSon array
    
    //MARK: -  for Text Theme Layer Method
    
    //MARK: -  Convert HTML text in Attributed string
    //MARK: -  Date Conversion
    func convertTimestamptoRelativeString(timestamp: String) -> Date {
        let msgDate = Date(timeIntervalSince1970: CDouble(timestamp)!)
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMMM d, yyyy"
        dateFormat.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        return dateFormat.date(from: dateFormat.string(from: msgDate))!
    }
    //MARK:- SetColor According To Theme
    func SetDarkBackgroundcolor() -> UIColor //303030
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.OffWhite
        }
        return Color
    }
    
    func SetLightBackgroundcolor() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.LightBlack
        }else{
            Color = Global.kAppColor.OffWhite
        }
        return Color
    }
    func SetLightBackgroundcolor_white() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.LightBlack
        }else{
            Color = Global.kAppColor.White
        }
        return Color
    }
    func SetSeperatorForTab() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.Seperator_Gray
        }
        return Color
    }
    func SetSerchviewColor() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.LightBlack
        }else{
            Color = Global.kAppColor.White
        }
        return Color
    }
    func SetLightBackgroundcolor_white_gray() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.OffWhite_search
        }
        return Color
    }

    func SetDarkBackgroundcolor_white() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.White
        }
        return Color
    }

    func SetTextcolor() -> UIColor //Black or white
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.White
        }else{
            Color = Global.kAppColor.Black
        }
        return Color
    }
    func SetTextFieldcolor() -> UIColor //Black or white
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.Blue
        }else{
            Color = Global.kAppColor.PlaceholderGray
        }
        return Color
    }
    func SetEmptyTextColor() -> UIColor //Black or white
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.White
        }else{
            Color = UIColor.darkGray
        }
        return Color
    }
    func SetHashTagColor() -> UIColor //Black or white
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.White
        }else{
            Color = UIColor.gray
        }
        return Color
    }
    func SetButtonsColor() -> UIColor //Black or white
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.Blue
        }else{
            Color = Global.kAppColor.ButtonGray
        }
        return Color
    }
    func SetSeperatorColor() -> UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.LightSeperator
        }
        return Color
    }
    func SetColorCreatePost() -> UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global.kAppColor.CreatePost
        }
        return Color
    }
    func SetSeperatorCreatePost() -> UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.LightBlack
        }else{
            Color = Global.kAppColor.White
        }
        return Color
    }

    func SetColorTextEmptyCreatePost() -> UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.White
        }else{
            Color = Global.kAppColor.ButtonGray
        }
        return Color
    }
    func SetColorTextFullCreatePost() -> UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.White
        }else{
            Color = Global.kAppColor.Black
        }
        return Color
    }
    func SetColorForDraft() ->UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.Blue
        }else{
            Color = Global.kAppColor.Black
        }
        return Color

    }
    func SetColorChattingText_sender() ->UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.Blue
        }else{
            Color = UIColor.clear
        }
        return Color
        
    }
    func SetColorChattingText_receiver() ->UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.ReceiverCell
        }else{
            Color = UIColor.clear
        }
        return Color
        
    }
    func SetColorForLoadingView() ->UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = UIColor.white
        }
        return Color
    }
    func SetColorForLoadingView_animation() ->UIColor{
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            Color = Global.kAppColor.DarkBlack
        }else{
            Color = Global().RGB(r: 230.0, g: 230.0, b: 230.0, a: 1.0)
        }
        return Color
    }
    func SetColorForSettingsButton_CGColor() ->CGColor{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return UIColor.white.cgColor
        }
          return UIColor.red.cgColor
    }
    func SetColorForSettingsButton() ->UIColor{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return UIColor.white
        }
        return UIColor.red
    }
    func SetColorForAddressButton_CGColor() ->CGColor{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return UIColor.white.cgColor
        }
        return Global.kAppColor.Blue.cgColor
    }
    func SetColorForAddressButton() ->UIColor{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return UIColor.white
        }
        return Global.kAppColor.Blue
    }

    func SetLightBackgroundcolor_audio() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global().RGB(r: 62.0, g: 82.0, b: 99.0, a: 1.0)
        }else{
            Color = Global.kAppColor.OffWhite
        }
        return Color
    }
    func SetColorForCartList_Plus() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = UIColor.white
        }else{
            Color = Global.kAppColor.OffWhite
        }
        return Color
    }
    func SetColorForCartList_Minus() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global.kAppColor.Gray
        }else{
            Color = UIColor.clear
        }
        return Color
    }
    func SetColorForEditScreenSeperator() -> UIColor //282828
    {
        var Color = UIColor()
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"
        {
            Color = Global().RGB(r: 18.0, g: 22.0, b: 31.0, a: 1.0)
        }else{
            Color = Global().RGB(r: 223.0, g: 222.0, b: 229.0, a: 1.0)
        }
        return Color
    }


    //MARK:- Set Images According To Theme
    
    func ArrowImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "DownArraow_Blue"
        }
        return "DownArraow"
    }

    func CartImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgBlankCart_white"
        }
        return "ImgBlankCart"
    }

    func CactusImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgFollow_white"
        }
        return "ImgFollow_white"
    }
    
    func CalenderImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "dark_calendar800"
        }
        return "calendar800"
    }
    func ChatImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgMessage_white"
        }
        return "ImgMessage_white"
    }
    func HistoryImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgHistory_white"
        }
        return "ImgHistory_white"
    }

    func CommentImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgComment_white"
        }
        return "ImgComment_white"
    }
    func NotificationImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgNotification_white"
        }
        return "ImgNotification_white"
    }
    func LikersImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgLikes_white"
        }
        return "ImgLikes_white"
    }
    func LockImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "dark_lock800"
        }
        return "lock800"
    }
    func PictureImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "dark_picture800"
        }
        return "picture800"
    }

    func PicturesImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgTimeline_White"
        }
        return "ImgTimeline_White"
    }
    func SearchImage() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgSearch_white"
        }
        return "ImgSearch_white"
    }
    func SearchImageBlank() -> String{
        if Global.singleton.retriveFromUserDefaults(key: "isDarkTheme") == "1"{
            return "ImgSearchBlank_white"
        }
        return "ImgSearchBlank_white"
    }

}
