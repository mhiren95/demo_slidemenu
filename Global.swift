//
//  Global.swift
//  chilax
//
//  Created by Tops on 6/13/17.
//  Copyright © 2017 Tops. All rights reserved.
//

import UIKit
// Code commited
extension Date {
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    func GetDay() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }

}
extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}
// for textfield placeholder color change
extension UITextField {
    func placeholderColor(_ color: UIColor){
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}

class Global {
    //SDK keys & secrets etc.
    struct SDKKeys {
        struct Twilio {
            static let Id = "AC313568df00fba35fcca607c6a9866d84"
            static let Secret = "1c024ca026674daf6e774901f809ca1d"
            static let FromNumber = "+447481344102"
            static let MsgURL = "https://api.twilio.com/2010-04-01/Accounts/" + Global.SDKKeys.Twilio.Id + "/Messages.json"
        }
        struct Adobe {
            static let ClientId = "35ecb47cec0448d4b2e75b415ff6ffff"
            static let Secret = "417bd5f4-5c57-44a1-b717-1ddd36f9d360"
        }
    }
    //Device Compatibility
    struct is_Device {
        static let _iPhone = (UIDevice.current.model as String).isEqual("iPhone") ? true : false
        static let _iPad = (UIDevice.current.model as String).isEqual("iPad") ? true : false
        static let _iPod = (UIDevice.current.model as String).isEqual("iPod touch") ? true : false
    }
    
    //Display Size Compatibility
    struct is_iPhone {
        static let _x = (UIScreen.main.bounds.size.height >= 812.0 ) ? true : false
        static let _6p = (UIScreen.main.bounds.size.height >= 736.0 ) ? true : false
        static let _6 = (UIScreen.main.bounds.size.height <= 667.0 && UIScreen.main.bounds.size.height > 568.0) ? true : false
        static let _5 = (UIScreen.main.bounds.size.height <= 568.0 && UIScreen.main.bounds.size.height > 480.0) ? true : false
        static let _4 = (UIScreen.main.bounds.size.height <= 480.0) ? true : false
    }
    
    //IOS Version Compatibility
    struct is_iOS {
        static let _10 = ((Float(UIDevice.current.systemVersion as String))! >= Float(10.0)) ? true : false
        static let _9 = ((Float(UIDevice.current.systemVersion as String))! >= Float(9.0) && (Float(UIDevice.current.systemVersion as String))! < Float(10.0)) ? true : false
        static let _8 = ((Float(UIDevice.current.systemVersion as String))! >= Float(8.0) && (Float(UIDevice.current.systemVersion as String))! < Float(9.0)) ? true : false
    }
    
    // MARK: -  Shared classes
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    static let singleton = Singleton.sharedSingleton
    
    
    
    // MARK: -  Screen size
    static let screenWidth : CGFloat = (Global.appDelegate.window!.bounds.size.width)
    static let screenHeight : CGFloat = (Global.appDelegate.window!.bounds.size.height)
    
    static let window = UIApplication.shared.keyWindow
    static let topPadding = window?.safeAreaInsets.top
    static let bottomPadding = window?.safeAreaInsets.bottom
    
    // MARK: -  Get UIColor from RGB
    func RGB(r: Float, g: Float, b: Float, a: Float) -> UIColor {
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue: CGFloat(b / 255.0), alpha: CGFloat(a))
    }
    
    // MARK: -  Dispatch Delay
    func delay(delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    // MARK: -  Web service Base URL
  //  static let baseURLPath = "http://topsdemo.co.in/webservices/chilax_app/public/api/v1/"
   // static let baseURLPath = "http://chilax.com/chilax_app_v3/public/api/v1/"
    //LIVE
   // static let baseURLPath = "http://chilax.co/public/api/v1/"
    
    //Parag's System
    
    //static let baseURLPath = "http://192.168.0.56/web_services/chilax_app/trunk/public/api/v1/"
     //Live URL
    //  static let baseURLPath = "https://search.chilax.co/chilax_app/public/api/v1/"
    //Main Live ...
    //static let baseURLPath = "https://chilax.co/chilax_app/public/apig/v1/"
    
    // TopsLive
  // static let baseURLPath = "https://topsdemo.co.in/webservices/chilax_backend/v1.3/api/"
    // ChilaxLive
  // static let baseURLPath = "https:/api.chilax.co/api/"
    // Live new
   static let baseURLPath = "https://www.chilax.co/dev_56/api/"

  // static let baseURLPath = "http://192.168.4.56/web_services/chilax_app/trunk/public/api/v1/"
   // static let baseURLPath = "http://192.168.0.56/web_services/chilax_app/trunk/public/api/v1/"
    static let baseURLPostShare = "https://www.chilax.co/"
   // static let baseURLPostShare = "https://www.chilax.co/dev_chilax/"
    // MARK: -  Application Colors
    struct kAppColor {
        static let CheckoutGreen = Global().RGB(r: 76.0, g: 217.0, b: 100.0, a: 1.0)

        static let Blue = Global().RGB(r: 0.0, g: 137.0, b: 244.0, a: 1.0)
        static let Gray = Global().RGB(r: 113.0, g: 113.0, b: 113.0, a: 1.0)
        static let Seperator_Gray = Global().RGB(r: 242.0, g: 242.0, b: 242.0, a: 1.0)

        static let DarkBlack = Global().RGB(r: 27.0, g: 39.0, b: 55.0, a: 1.0)
        static let LightBlack = Global().RGB(r: 21.0, g: 30.0, b: 41.0, a: 1.0)
        static let OffWhite = Global().RGB(r: 239.0, g: 239.0, b: 244.0, a: 1.0)
        static let OffWhite_search = Global().RGB(r: 229.0, g: 229.0, b: 234.0, a: 1.0)

        static let White = UIColor.white
        static let Black = Global().RGB(r: 40.0, g: 40.0, b: 40.0, a: 1.0)
        static let DarkSeperator = Global().RGB(r: 71.0, g: 71.0, b: 71.0, a: 1.0)
        static let LightSeperator = Global().RGB(r: 229.0, g: 229.0, b: 234.0, a: 1.0)
        static let Gray_black = Global().RGB(r: 57.0, g: 57.0, b: 57.0, a: 1.0)
        static let off_white = Global().RGB(r: 242.0, g: 242.0, b: 243.0, a: 1.0)
        static let ButtonGray = Global().RGB(r: 142.0, g: 142.0, b: 147.0, a: 1.0)
        static let PlaceholderGray = Global().RGB(r: 142.0, g: 141.0, b: 147.0, a: 1.0)
        static let CreatePost = Global().RGB(r: 245.0, g: 244.0, b: 244.0, a: 1.0)
        static let ReceiverCell = Global().RGB(r: 66.0, g: 83.0, b: 100.0, a: 1.0)

    }

    // MARK: -  Application Name
    static let kAppName = "chilax"
    
    // MARK: -  Application Fonts
    struct kFont {
        //static let GothamBold = "Gotham-Bold"
        static let GothamBold = "Gotham-Bold"
        static let ArabicBold = "Arabic-Bold"
        static let AraboicRegular = "Arabic-Regular"
        static let GothamBook = "Gotham-Book"
        static let GothamRegular = "Gotham-Regular"
        static let GothamMedium = "Gotham-Medium"
        static let GothamLight = "Gotham-Light"
        static let ChilaxIcon = "Chilax"
        static let ChilaxNewIcon = "Chillax_TOPS"
        static let Chilax_New = "chilax_NEW"
        static let ChilaxIcons = "ChilaxIcons"

    }
    struct kFont_system {
        //static let GothamBold = "Gotham-Bold"
        static let System_HeavyFont_34 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(32.0), weight: .bold)
        static let System_Regular_17 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(17.0), weight: .regular)
        static let System_Regular_15 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(15.0), weight: .regular)
        static let System_Semibold_15 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(15.0), weight: .semibold)
        static let System_Semibold_13 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(13.0), weight: .semibold)

        static let System_SemiBold_17 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(17.0), weight: .semibold)
        static let System_Regular_10 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(10.0), weight: .regular)
        static let System_Regular_08 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(10.0), weight: .regular)

        static let System_Regular_12 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(12.0), weight: .regular)
        static let System_Regular_14 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(14.0), weight: .regular)
        static let System_Semibold_14 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(14.0), weight: .semibold)

        static let System_Semibold_12 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(12.0), weight: .semibold)

        static let System_Regular_22 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(22.0), weight: .regular)
        static let System_regular_20 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(20.0))
        static let System_SemiBold_22 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(22.0), weight: .semibold)
        static let System_SemiBold_28 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(28.0), weight: .bold)
        static let System_Regular_13 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(13.0), weight: .regular)
        static let System_Regular_20 = UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize_2(20.0), weight: .regular)

//        static let AraboicRegular = "Arabic-Regular"
//        static let GothamBook = "Gotham-Book"
//        static let GothamRegular = "Gotham-Regular"
//        static let GothamMedium = "Gotham-Medium"
//        static let GothamLight = "Gotham-Light"
//        static let ChilaxIcon = "Chilax"
//        static let ChilaxNewIcon = "Chillax_TOPS"
//        static let Chilax_New = "chilax_NEW"
        
    }
    // MARK: -  User Data
    struct kLoggedInUserKey {
        static let IsLoggedIn: String! = "chilaxUserIsLoggedIn"
        static let Id: String! = "chilaxUserId"
        static let FullName: String! = "chilaxUserFullName"
        static let FirstName: String! = "chilaxUserFName"
        static let LastName: String! = "chilaxUserLName"
        static let UserName: String! = "chilaxUserName"
        static let Email: String! = "chilaxUserEmail"
        static let Gender: String! = "chilaxUserGender"
        static let MobileNumber: String! = "chilaxUserMobileNumber"
        static let Phone: String! = "chilaxUserPhone"
        static let CountryCode: String! = "chilaxUserCountryCode"
        static let CountryName: String! = "chilaxUserCountryName"
        static let Biodata: String! = "chilaxUserBiodata"
        static let Website: String! = "chilaxUserWebsite"
        static let DOB: String! = "chilaxUserDOB"
        static let ProfilePic: String! = "chilaxUserProfilePic"
        static let AccessToken: String! = "chilaxUserAccessToken"
        static let FirstTimeLogin: String! = "chilaxUserFirstTimeLogin"
        static let Birthdate:String! = "chilaxUserBirthdate"
        static let is_Privat: String! = "chilaxUserIsPrivat"
        static let is_Varify: String! = "chilaxUserIsverify"
        static let NeedAuthentication: String! = "chilaxNeedAuthentication"
        
        static let download_image: String! = "chilaxdownload_off"
        static let is_merchant: String! = "chilaxis_merchant"
        static let CustServ_Email: String! = "chilaxCustServ_Email"
        static let CustServ_Contact: String! = "chilaxCustServ_Contact"

        
        static let followers: String! = "chilaxUserIsfollower"
        static let following: String! = "chilaxUserIsfollowing"
        
        static let noti_followerFlag: String! = "chilaxUserfollowerFlag"
        static let noti_MsgFlag: String! = "chilaxUserMsgFlag"
        static let noti_NewPostFlag: String! = "chilaxUserNewPostFlag"
        //static let noti_PrivateFlag: String! = "chilaxUserPrivateFlag"
        static let noti_MentionFlag: String! = "chilaxUserMentionFlag"
        
        static let openFirePasswd: String! = "XMPPOpenFirePassword"
        static let openFireUserName: String! = "XMPPOpenFireUserName"
    }
    
    struct kLoggedInUserData {
        var IsLoggedIn: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.IsLoggedIn)
        var NeedAuthentication: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.NeedAuthentication)
        var download_image: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.download_image)
        
        var is_merchant: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.is_merchant)

        var CustServ_Email: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.CustServ_Email)
        
        var CustServ_Contact: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.CustServ_Contact)

        
        var Id: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Id)
        var FullName: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.FullName)
        var UserName: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.UserName)
        var Email: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Email)
        var Gender: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Gender)
        var MobileNumber: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.MobileNumber)
        var Phone: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Phone)
        var CountryCode: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.CountryCode)
        var CountryName: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.CountryName)
        var Biodata: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Biodata)
        var Website: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.Website)
        var DOB: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.DOB)
        var ProfilePic: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.ProfilePic)
        var AccessToken: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.AccessToken)
        var FirstTimeLogin: String? = Global.singleton.retriveFromUserDefaults(key: Global.kLoggedInUserKey.FirstTimeLogin)
    }

    // MARK: -  String Type for Validation
    enum kStringType : Int {
        case AlphaNumeric
        case AlphabetOnly
        case NumberOnly
        case Fullname
        case Username
        case Email
        case PhoneNumber
    }
    
    // MARK: -  Post Media Type
    struct kPostMediaType {
        static let text: String! = "text"
        static let image: String! = "image"
        static let video: String! = "video"
        static let audio: String! = "audio"
    }
    
    // MARK: -  Create Post: Valid Image Ratio
    static let arrPostImageRatio: NSArray = NSArray(objects: "1", "1.00", "1.34", "1.51", "1.41", "1.26", "1.78", "1.77", "0.76", "0.67", "0.72", "0.81", "0.57", "1.33", "1.50", "1.40", "1.25", "1.75", "0.75", "0.66", "0.71", "0.80", "0.56", "0.57")
    
    // MARK: -  Create Post: Text Theme Colors
    struct kTextThemeColor {
        static let Start_1 = Global().RGB(r: 248, g: 248, b: 141, a: 1).cgColor
        static let End_1 = Global().RGB(r: 248, g: 248, b: 141, a: 1).cgColor
        
        static let Start_2 = Global().RGB(r: 86, g: 229, b: 159, a: 1).cgColor
        static let End_2 = Global().RGB(r: 40, g: 187, b: 230, a: 1).cgColor
        
        static let Start_3 = Global().RGB(r: 74, g: 144, b: 226, a: 1).cgColor
        static let End_3 = Global().RGB(r: 74, g: 144, b: 226, a: 1).cgColor
        
        static let Start_4 = Global().RGB(r: 220, g: 56, b: 246, a: 1).cgColor
        static let End_4 = Global().RGB(r: 97, g: 63, b: 219, a: 1).cgColor
        
        static let Start_5 = Global().RGB(r: 243, g: 83, b: 105, a: 1).cgColor
        static let End_5 = Global().RGB(r: 243,g: 83, b: 105, a: 1).cgColor
        
        static let Start_6 = Global().RGB(r: 252, g: 209, b: 114, a: 1).cgColor
        static let End_6 = Global().RGB(r: 244, g: 89, b: 106, a: 1).cgColor
        
        static let Start_7 = Global().RGB(r: 137, g: 250, b: 147, a: 1).cgColor
        static let End_7 = Global().RGB(r: 137, g: 250, b: 147, a: 1).cgColor
        
        static let Start_8 = Global().RGB(r: 255, g: 150, b: 225, a: 1).cgColor
        static let End_8 = Global().RGB(r: 255, g: 150, b: 225, a: 1).cgColor
    }
    
    struct kTimelinePostHeight {
        static let ImageHeight: Float = 240
        static let VideoHeight: Float = 240
        static let TextHeight: Float = Float(Global.screenHeight) - Float(20) - ((Float(Global.screenWidth) * 44) / 320)
        static let AudioHeight: Float = 165
    }
    static func SetAlertFont(str:String) -> NSMutableAttributedString{
        let messageFont = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: Global.singleton.getDeviceSpecificFontSize(14.0))]
        let messageAttrString = NSMutableAttributedString(string: str, attributes: messageFont)
        return messageAttrString
    }


    static func getDocumentDirectoryPath() -> URL {
        
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory,
                                            in: .userDomainMask)
        guard documentsUrl.count != 0 else {
            return URL.init(string: "")!// Could not find documents URL
        }
        return documentsUrl.first!
    }
    static func GetTimeZone() -> (String,String){

        let timezone = NSTimeZone.local
        let strUTCTime = timezone.abbreviation()
        let strTimeZone = timezone.identifier
        
        return (strTimeZone,strUTCTime!)
    }
    static func CheckLanguage(str:String) -> String{
            if #available(iOS 11.0, *) {
            if let language = NSLinguisticTagger.dominantLanguage(for: str) {
                print(language)
                return language
            }
            else {
                return "en"
            }
    }
        return " "
}
    
    static func getDocumentDirectoryVideoPath(strPath:String) -> URL {
        
        let fileManager = FileManager.default
        let documentsUrl = fileManager.urls(for: .documentDirectory,
                                            in: .userDomainMask)
        guard documentsUrl.count != 0 else {
            return URL.init(string: "")!// Could not find documents URL
        }
        return documentsUrl.first!.appendingPathComponent(strPath)
    }
    
    static func isFileExistInDocumentDir(strFileName:String) -> Bool {
        
        let documentsURL = try! FileManager().url(for: .documentDirectory,in: .userDomainMask,appropriateFor: nil,create: true)
        let fooURL = documentsURL.appendingPathComponent(strFileName)
        return FileManager().fileExists(atPath: fooURL.path)
    }
    static func AddSpacetoLeft() -> UIView
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 17.5, height: 30))
        return paddingView
    }

    //Change By Chirag
    static func getDocumentFDirFileExistInFolderPath(strFolderName:String) -> URL {
        
        let documentsURL = try! FileManager().url(for: .documentDirectory,in: .userDomainMask,appropriateFor: nil,create: true)
        let fooURL = documentsURL.appendingPathComponent(strFolderName)
        if !FileManager.default.fileExists(atPath: fooURL.path) {
            try! FileManager.default.createDirectory(atPath: fooURL.path, withIntermediateDirectories: true, attributes: nil)
        }
        return fooURL
    }
}
